function setbg(color)
{
    document.getElementById("styled").style.background=color
}

$(document).ready(function() {

    $container = $('body');

    $container.delegate('.new-post .post-item > button', 'click touchstart', function(e) {
        if(!$(this).hasClass('active')) {
            $("#buttons-post-list li > button").removeClass('active');
           // $("#write-area").not('.hidden').addClass("hidden");
            $('*[data="post"]').not('.hidden').addClass("hidden");
            var id= $(this).parent().attr('id').substring(3);
            $('#'+id ).removeClass('hidden').fadeIn('slow');;
            $(this).addClass('active');
        }

    });

$container.delegate('a#close-input', 'click touchstart', function(e) {
    $("#buttons-post-list li > button").removeClass('active');
    $('*[data="post"]').not('.hidden').addClass("hidden");
});

    $container.delegate('a#submit-input', 'click', function(e) {
        $("#buttons-post-list li > button").removeClass('active');
        $('*[data="post"]').not('.hidden').addClass("hidden");
    });

    $('.search-navigation').hover(function(){
        //show the box
        $('.user-search').fadeIn(300);
    }, function() {
        $('.user-search').fadeOut(300);
    });

});